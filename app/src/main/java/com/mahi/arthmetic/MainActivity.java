package com.mahi.arthmetic;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;


import com.mahi.arthmetic.databinding.ActivityMainBinding;
import com.mahi.arthmetic.model.ProductModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    ProductviewModel mainViewModel;
    AdpterFeed feed;
    ActivityMainBinding activityMainBinding;

    private ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dialog = new ProgressDialog(MainActivity.this);

         activityMainBinding= DataBindingUtil.setContentView(this, R.layout.activity_main);


        activityMainBinding.recyclerview.setLayoutManager(new LinearLayoutManager(this));
        activityMainBinding.recyclerview.setHasFixedSize(true);
        mainViewModel = ViewModelProviders.of(this).get(ProductviewModel.class);

        getData();



    }
    private void getData() {

        dialog.setMessage("Please wait..");
        dialog.show();
        mainViewModel.getCategory().observe(this, new Observer<List<ProductModel>>() {
            @Override
            public void onChanged(@Nullable List<ProductModel> employees) {

                Log.e("hell2","size="+employees.size());
                feed = new AdpterFeed(MainActivity.this,employees);


                activityMainBinding.recyclerview.setAdapter(feed);

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }



            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_contact, menu);

        final MenuItem searchItem = menu.findItem(R.id.search);

        SearchManager searchManager = (SearchManager) MainActivity.this.getSystemService(Context.SEARCH_SERVICE);





        final SearchView finalSearchView = (SearchView) menu.findItem(R.id.search).getActionView();



        if (finalSearchView != null) {

            finalSearchView.setSearchableInfo(
                    searchManager.getSearchableInfo(getComponentName()));


            finalSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {


                    // Toast like print
                    //UserFeedback.show( "SearchOnQueryTextSubmit: " + query);
                    Log.e("SearchOnQueryTex", "s=" + query);
                    if (!finalSearchView.isIconified()) {
                        finalSearchView.setIconified(true);
                    }
                    searchItem.collapseActionView();
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    // UserFeedback.show( "SearchOnQueryTextChanged: " + s);

                    Log.e("SearchOnQueryTextChang ", "s=" + s);

                    feed.getFilter().filter(s);
                    return true;
                }
            });
        }
        return super.onCreateOptionsMenu(menu);
    }

}