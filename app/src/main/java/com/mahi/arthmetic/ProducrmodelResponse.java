package com.mahi.arthmetic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mahi.arthmetic.model.ProductModel;

import java.util.ArrayList;
import java.util.List;

public class ProducrmodelResponse {

    @SerializedName("resultCount")
    @Expose
    private Integer resultCount;


    public Integer getResultCount() {
        return resultCount;
    }

    public void setResultCount(Integer resultCount) {
        this.resultCount = resultCount;
    }

    public List<ProductModel> getResults() {
        return results;
    }

    public void setResults(List<ProductModel> results) {
        this.results = results;
    }

    @SerializedName("results")
    @Expose
    private List<ProductModel> results  = new ArrayList<>();

}
