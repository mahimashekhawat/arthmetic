
package com.mahi.arthmetic.model;


import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductColor implements Parcelable
{

    @SerializedName("hex_value")
    @Expose
    private String hexValue;
    @SerializedName("colour_name")
    @Expose
    private String colourName;
    public final static Creator<ProductColor> CREATOR = new Creator<ProductColor>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ProductColor createFromParcel(android.os.Parcel in) {
            return new ProductColor(in);
        }

        public ProductColor[] newArray(int size) {
            return (new ProductColor[size]);
        }

    }
    ;

    protected ProductColor(android.os.Parcel in) {
        this.hexValue = ((String) in.readValue((String.class.getClassLoader())));
        this.colourName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ProductColor() {
    }

    public String getHexValue() {
        return hexValue;
    }

    public void setHexValue(String hexValue) {
        this.hexValue = hexValue;
    }

    public String getColourName() {
        return colourName;
    }

    public void setColourName(String colourName) {
        this.colourName = colourName;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(hexValue);
        dest.writeValue(colourName);
    }

    public int describeContents() {
        return  0;
    }

}
