package com.mahi.arthmetic;

import android.content.Context;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Filter;
import android.widget.Filterable;



import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mahi.arthmetic.databinding.ListItemBinding;
import com.mahi.arthmetic.model.ProductModel;

import java.util.ArrayList;
import java.util.List;

public class AdpterFeed extends RecyclerView.Adapter<AdpterFeed.ViewHolder> implements Filterable {


    public List<ProductModel> memberlist_node=new ArrayList<ProductModel>();
    List<ProductModel> filteredList = new ArrayList<ProductModel>();
    Context context;




    public AdpterFeed(Context context, List<ProductModel> memberlist_node) {
        this.context = context;
        this.memberlist_node = memberlist_node;
        this.filteredList=memberlist_node;

    }





    @NonNull
    @Override
    public AdpterFeed.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        ListItemBinding ListItemBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.list_item, parent, false);
        return new ViewHolder(ListItemBinding);

    }

    @Override
    public void onBindViewHolder(@NonNull AdpterFeed.ViewHolder holder, final int position) {



                 final ProductModel model= filteredList.get(position);

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_launcher_round);

        Glide.with(context).load(model.getImageLink()).thumbnail(Glide.with(context).load(R.drawable.ic_launcher_background)).apply(options).into(holder.listItemBinding.ivPic);
       holder.listItemBinding.setAlbum(model);

       holder.listItemBinding.cvEmployee.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Log.e("click","click"+position);

               Intent as = new Intent(context,DetailActivity.class);


               Log.e("click","click"+model.getName());
               as.putExtra("product", filteredList.get(position));
               context.startActivity(as);

           }
       });








    }

    @Override
    public int getItemCount() {

        return  filteredList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        private ListItemBinding listItemBinding;
        public ViewHolder(@NonNull ListItemBinding listItemBinding) {
            super(listItemBinding.getRoot());
            this.listItemBinding = listItemBinding;
        }
    }


    //

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredList = memberlist_node;
                } else {
                    List<ProductModel> filteredListlatest = new ArrayList<>();
                    for (ProductModel row : memberlist_node) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) ) {
                            filteredListlatest.add(row);
                        }
                    }
                    if(filteredListlatest.size()>0) {

                        filteredList = filteredListlatest;
                    }
                    else
                    {
                        filteredList = memberlist_node;
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredList = (ArrayList<ProductModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    //

}
