package com.mahi.arthmetic;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.mahi.arthmetic.databinding.ActivityDetailBinding;
import com.mahi.arthmetic.model.ProductModel;

public class DetailActivity extends AppCompatActivity {

    ActivityDetailBinding activityDetailBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityDetailBinding= DataBindingUtil.setContentView(this, R.layout.activity_detail);


        ProductModel model=getIntent().getParcelableExtra("product");

        Log.e("detail","detail"+model.getName());

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_launcher_round);

        Glide.with(DetailActivity.this).load(model.getImageLink()).thumbnail(Glide.with(DetailActivity.this).load(R.drawable.ic_launcher_background)).apply(options).into(activityDetailBinding.ivPic);

        activityDetailBinding.setAlbum(model);
    }
}