package com.mahi.arthmetic.Application;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;

import android.util.Log;
import android.widget.ImageView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;



public class MyUIApplication extends Application {



    public static final String TAG = MyUIApplication.class.getSimpleName();

    private static MyUIApplication instance;

    Activity act;
    Gson gson;

    // Milliseconds per second
    private static final int MILLISECONDS_PER_SECOND = 1000;
    // Update frequency in seconds
    public static final int UPDATE_INTERVAL_IN_SECONDS = 10;
    // Update frequency in milliseconds

    private static final long UPDATE_INTERVAL =
            MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    // The fastest update frequency, in seconds
    private static final int FASTEST_INTERVAL_IN_SECONDS = 10;
    // A fast frequency ceiling in milliseconds
    private static final long FASTEST_INTERVAL =
            MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;






    public MyUIApplication() {
        instance = this;
    }


    //
    @Override
    protected void attachBaseContext(Context base) {

        super.attachBaseContext(base);

    }

    public static Context getContext() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();








        //
    }


    public Gson getGson() {
        if (gson != null) {
            return gson;
        } else {

            GsonBuilder gsonBuilder = new GsonBuilder();
            gson = gsonBuilder.create();
            return gson;
        }
    }


    public static synchronized MyUIApplication getInstance() {
        return instance;
    }


}
