package com.mahi.arthmetic.Retrofit;


import com.mahi.arthmetic.ProducrmodelResponse;
import com.mahi.arthmetic.model.ProductModel;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Arun
 */

public interface APIInterface {

  @GET("api/v1/products.json")
    Call<List<ProductModel>> doGetListResources(@Query("product_type") String product_type);




}
