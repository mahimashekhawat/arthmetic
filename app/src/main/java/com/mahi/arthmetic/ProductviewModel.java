package com.mahi.arthmetic;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mahi.arthmetic.Application.MyUIApplication;
import com.mahi.arthmetic.Retrofit.APIClient;
import com.mahi.arthmetic.Retrofit.APIInterface;
import com.mahi.arthmetic.model.ProductModel;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductviewModel extends ViewModel {

    //this is the data that we will fetch asynchronously
    MutableLiveData<List<ProductModel>> categories;
    APIInterface apiInterface;

//
    //we will call this method to get the data
    public LiveData<List<ProductModel>> getCategory() {
        //if the list is null
        if (categories == null) {
            categories = new MutableLiveData<>();
            //we will load it asynchronously from server in this method
            loadCategory();

            Log.e("datalog","dataloader");
        }

        //finally we will return the list
        return categories;
    }


    //This method is using data to get the JSON data from assests
    private void loadCategory() {

                   // categories.setValue();


        //
        apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<List<ProductModel>> call = apiInterface.doGetListResources("nail_polish");
        //Call<User> call1 = apiInterface.createUser(editText.getText().toString(), editText1.getText().toString(),"adad","2");
        call.enqueue(new Callback<List<ProductModel>>() {
            @Override
            public void onResponse(Call<List<ProductModel>> call, Response<List<ProductModel>> response) {
               // User user1 = response.body();
                Log.e("response","response="+response.body().toString());

               // Log.e("user1","user1="+response.body().toString());


               // Intent as = new Intent(MainActivity.this,Productshow.class);
                //startActivity(as);

                //Toast.makeText(getApplicationContext(), user1.name + " " + user1.job + " " + user1.id + " " + user1.createdAt, Toast.LENGTH_SHORT).show();


                List<ProductModel> productlist_nodem =response.body();
                Log.e("hell","size="+productlist_nodem.size());
                categories.setValue(productlist_nodem);

            }

            @Override
            public void onFailure(Call<List<ProductModel>> call, Throwable t) {
              //  call.cancel();

                Log.e("trace","trace="+t.getStackTrace());
            }


        });
        //

       /* String data = Utils.getAssetJsonData(MyUIApplication.getInstance());
        Type type = new TypeToken<ProducrmodelResponse>(){}.getType();
       ProducrmodelResponse properties = new Gson().fromJson(data, type);

        List<ProductModel> productlist_nodem = properties.getResults();
        Log.e("hell","size="+productlist_nodem.size());
        categories.setValue(productlist_nodem);*/

    }




}